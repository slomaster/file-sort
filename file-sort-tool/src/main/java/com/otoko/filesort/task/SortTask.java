package com.otoko.filesort.task;

import java.util.LinkedList;

/**
 * Задача на сортировку файла.
 */
public class SortTask {

    private String status;
    private final Object statusLock = new Object();
    private final String fileName;
    private final String tempFileName;

    private final LinkedList<SortTaskListener> listeners;

    /**
     * Конструктор.
     *
     * Во время сортировки будет задействован временный файл такого же размера как и сортируемый.
     *
     * @param fileName Имя файла.
     * @param fileTempName Имя временного файла.
     */
    public SortTask(String fileName, String fileTempName) {
        this.fileName = fileName;
        this.tempFileName = fileTempName;
        status = "Ready";
        listeners = new LinkedList<>();
    }

    /**
     * Добавление подписчика на оповещения о завершении сортировки.
     *
     * @param listener Подписчик на оповещения.
     */
    public void addListener(SortTaskListener listener) {
        listeners.add(listener);
    }

    /**
     * Оповещение подписчиков.
     */
    public void finish() {
        for (SortTaskListener listener : listeners) {
            listener.taskFinished(this);
        }
    }

    public String getFileName() {
        return fileName;
    }

    public String getTempFileName() {
        return tempFileName;
    }

    /**
     * Установить статус.
     *
     * TODO Расписать про потокобезопасность метода.
     *
     * @param status Статус.
     */
    public void setStatus(String status) {
        synchronized (statusLock) {
            this.status = status;
        }
    }

    /**
     * Получить статус.
     *
     * TODO Расписать про потокобезопасность метода.
     *
     * @return Статус.
     */
    public String getStatus() {
        synchronized (statusLock) {
            return status;
        }
    }
}