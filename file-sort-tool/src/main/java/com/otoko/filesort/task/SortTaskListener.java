package com.otoko.filesort.task;

/**
 * Интерфейс подписчика на событие завершения сортировки.
 */
public interface SortTaskListener {

    void taskFinished(SortTask sortTask);
}
