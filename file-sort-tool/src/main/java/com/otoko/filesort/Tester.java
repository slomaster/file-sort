package com.otoko.filesort;

import com.otoko.filesort.util.FileUtils;
import com.otoko.filesort.writer.CharFileWriter;
import com.otoko.filesort.writer.FloatFileWriter;
import com.otoko.filesort.writer.ParallelFloatFileWriter;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;

import static com.otoko.filesort.util.FileUtils.elementSizeInBytes;
import static com.otoko.filesort.util.TimeUtils.getTimeString;

class Tester {

    // for tests and debug
    private static void generateRandomGbFile(int numOfGb, String fileName) {
        try {
            long timeStart = System.currentTimeMillis();

            RandomAccessFile file = new RandomAccessFile(fileName, "rw");
            file.setLength(0);
            Random rand = new Random();
            byte[] buffer = new byte[elementSizeInBytes * 32768]; // 128K
            for (int i = 0; i < 8192 * numOfGb; i++) {
                rand.nextBytes(buffer);
                file.write(buffer);
            }
            file.close();

            long timeFinish = System.currentTimeMillis();

            System.out.println("Generation duration: "
                    + getTimeString(timeFinish-timeStart));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // for tests and debug
    private static void generateRandomFloatFile(int numOfFloats, String fileName) {
        try {
            long timeStart = System.currentTimeMillis();

            RandomAccessFile file = new RandomAccessFile(fileName, "rw");
            file.setLength(0);
            FloatFileWriter writer = new ParallelFloatFileWriter(file);
            Random rand = new Random();
            for (int i = 0; i < numOfFloats; i++) {
                writer.writeFloat(rand.nextFloat());
            }
            writer.close();
            file.close();

            long timeFinish = System.currentTimeMillis();

            System.out.println("Generation duration: "
                    + getTimeString(timeFinish-timeStart));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // for tests and debug
    private static void generateRandomCharFile(int numOfChars, String fileName) {
        char[] numbers = {'0','1','2','3','4','5','6','7','8','9'};
        try {
            RandomAccessFile file = new RandomAccessFile(fileName, "rw");
            file.setLength(0);
            CharFileWriter writer = new CharFileWriter(file);
            Random rand = new Random();
            char c;
            for (int i = 0; i < numOfChars; i++) {
                c = numbers[Math.abs(rand.nextInt()) % 10];
                System.out.print(c + "" + c + "" + "" + c + "" + c);
                writer.writeChar(c);
                writer.writeChar(c);
                writer.writeChar(c);
                writer.writeChar(c);
            }
            System.out.println();
            writer.close();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

//        generateRandomCharFile(31, "file.txt");
        generateRandomGbFile(1,"file.txt");
//        generateRandomFloatFile(268435456,"file.txt"); //1Gb

        FileUtils.resetDurations();

//        FloatFileSorter fileSorter = new FloatFileSorter();
//        try {
//            fileSorter.sortFile(new SortTask("file.txt", "temp.txt"));
//        } catch (SortException e) {
//            e.printStackTrace();
//        }

//        MultitaskFloatFileSorter multitaskFloatFileSorter = new MultitaskFloatFileSorter();
//        multitaskFloatFileSorter.sortFile("file.txt", "temp.txt");
//        multitaskFloatFileSorter.shutdown();

        FileUtils.printDurations();
    }
}