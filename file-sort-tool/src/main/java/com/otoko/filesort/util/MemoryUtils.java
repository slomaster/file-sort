package com.otoko.filesort.util;

/**
 * Набор методов и констант для определения размеров и количества структур данных под задачу сортировки файла.
 */
public class MemoryUtils {

    private MemoryUtils() {
    }

    public static int getMaxReadBufferSize() {
        return maxReadBufferSize;
    }

    public static int getMaxWriteBufferSize() {
        return defaultMaxWriteBufferSize;
    }

    public static int getMemLimitBufferSize() {
        return defaultMemLimitBufferSize;
    }

    public static int getMaxBlockSizePerCore() {
        return maxBlockSizePerCore;
    }

    public static int getMaxBlockSizeTotal() {
        return maxBlockSizeTotal;
    }

    private static final int defaultMaxReadBufferSize = 32768; //128K
    private static final int defaultMaxWriteBufferSize = 32768; //128K
    private static final int defaultMemLimitBufferSize = 32768; //128K

    private static final int defaultMaxBlockSizePerCore = 262144 * 40; //40M
    private static final int defaultMaxBlockSizeTotal = 262144 * 40; //40M

    private static int maxReadBufferSize = defaultMaxReadBufferSize;

    private static int maxBlockSizePerCore = defaultMaxBlockSizePerCore;
    private static int maxBlockSizeTotal = defaultMaxBlockSizeTotal;

    private static final long defaultMaxMemoryLimit = 2 * 1024 * 1024 * 1024L; //2Gb

    private static long totalMemory;
    private static long maxMemory;
    private static long freeMemory;
    private static long memoryInUse;

    public static void updateLimits() {
        // Get an instance of the Runtime class
        Runtime runtime = Runtime.getRuntime();

        // Memory which is currently available for use by heap
        totalMemory = runtime.totalMemory();

        // Maximum memory which can be used if required.
        // The heap cannot grow beyond this size
        maxMemory = runtime.maxMemory();

        // Free memory still available
        freeMemory = runtime.freeMemory();

        // Memory currently used by heap
        memoryInUse = totalMemory - freeMemory;
    }

    public static long getTotalMemory() {
        return totalMemory;
    }

    public static long getMaxMemory() {
        return maxMemory;
    }

    public static long getFreeMemory() {
        return freeMemory;
    }

    public static long getMemoryInUse() {
        return memoryInUse;
    }

    public static void printValues() {
        java.io.PrintStream out = System.out;

        out.println("maxReadBufferSize size: " + maxReadBufferSize + " floats ( " +
                String.format("%.2f", maxReadBufferSize / (1024 * 1024.0)) + " MB )");
        out.println("defaultMaxWriteBufferSize size: " + defaultMaxWriteBufferSize + " floats ( " +
                String.format("%.2f", defaultMaxWriteBufferSize / (1024 * 1024.0)) + " MB )");
        out.println("defaultMemLimitBufferSize size: " + defaultMemLimitBufferSize + " floats ( " +
                String.format("%.2f", defaultMemLimitBufferSize / (1024 * 1024.0)) + " MB )");
        out.println("maxBlockSizePerCore size: " + maxBlockSizePerCore + " floats ( " +
                String.format("%.2f", maxBlockSizePerCore / (1024 * 1024.0)) + " MB )");
        out.println("maxBlockSizeTotal size: " + maxBlockSizeTotal + " floats ( " +
                String.format("%.2f", maxBlockSizeTotal / (1024 * 1024.0)) + " MB )");
    }

    public static void printLimits() {
        updateLimits();

        // To convert from Bytes to MegaBytes:
        // 1 MB = 1024 KB and 1 KB = 1024 Bytes.
        // Therefore, 1 MB = 1024 * 1024 Bytes.
        long MegaBytes = 1024 * 1024;

        java.io.PrintStream out = System.out;
        out.println("Heap size available for use -> " + totalMemory / MegaBytes + " MB");
        out.println("Maximum memory Heap can use -> " + maxMemory / MegaBytes + " MB");
        out.println("Free memory in heap -> " + freeMemory / MegaBytes + " MB");
        out.println("Memory already used by heap -> " + memoryInUse / MegaBytes + " MB");
    }

    public static void calculateValues(long fileLength) {
        calculateValues(fileLength, defaultMaxMemoryLimit);
    }

    /**
     * Расчёт количества и размера основных структур исходя из сведений о размере сортируемого файла и ограничений по
     * памяти.
     *
     * @param fileLength     размер файла.
     * @param maxMemoryLimit максимальное количество памяти, которое позволено занять.
     */
    public static void calculateValues(long fileLength, long maxMemoryLimit) {
        updateLimits();
        printLimits();

        long dataLength = fileLength / FileUtils.elementSizeInBytes;

        long availableMemory =
                Long.min(maxMemoryLimit, (maxMemory - memoryInUse - 1024 * 1024)) / FileUtils.elementSizeInBytes;

        maxBlockSizeTotal = Math.toIntExact(Long.min(availableMemory / 3, Integer.MAX_VALUE));

        if (dataLength / 2 < maxBlockSizeTotal)
            maxBlockSizeTotal = Math.toIntExact(dataLength / 2 + 1);

        maxBlockSizePerCore = maxBlockSizeTotal;

        int blocksNumber = Math.toIntExact(dataLength / maxBlockSizeTotal);
        if (dataLength % maxBlockSizeTotal > 0)
            blocksNumber++;

        maxReadBufferSize =
                Math.toIntExact(Long.min(defaultMaxReadBufferSize,
                        (availableMemory - (defaultMaxWriteBufferSize * 2)) / (blocksNumber * 2)));

        printValues();
    }
}
