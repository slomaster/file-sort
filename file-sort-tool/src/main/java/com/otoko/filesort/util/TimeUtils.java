package com.otoko.filesort.util;

/**
 * Набор методов для работы со временем.
 */
public class TimeUtils {

    private TimeUtils() {
    }

    /**
     * Конвертация времени в читаемую строку.
     *
     * @param durationInMillis время в миллисекундах.
     * @return форматированная строка.
     */
    public static String getTimeString(long durationInMillis) {
        long millis = durationInMillis % 1000;
        long second = (durationInMillis / 1000) % 60;
        long minute = (durationInMillis / (1000 * 60)) % 60;
        long hour = (durationInMillis / (1000 * 60 * 60)) % 24;
        return String.format("%02d:%02d:%02d.%d", hour, minute, second, millis);
    }

    /**
     * Конвертация времени в читаемую строку.
     *
     * @param durationInNanos время в наносекундах.
     * @return форматированная строка.
     */
    public static String getTimeStringNano(long durationInNanos) {
        return getTimeString(durationInNanos / 1000000);
    }
}