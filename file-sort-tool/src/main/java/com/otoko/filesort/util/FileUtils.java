package com.otoko.filesort.util;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

import static com.otoko.filesort.util.TimeUtils.getTimeStringNano;

/**
 * Набор методов для работы с RandomAccessFile. Встроенное профилирование и ограничения по памяти.
 */
public class FileUtils {

    private FileUtils() {
    }

    public final static int elementSizeInBytes = 4; // size of Float

    private static int memLimitBufferSize = 32768; // 128K

    public static void setMemLimitBufferSize(int memLimitBufferSize) {
        FileUtils.memLimitBufferSize = memLimitBufferSize;
    }

    static private long durationRead = 0;
    static private long durationWrite = 0;
    static private long durationConvert = 0;

    public static void resetDurations() {
        long durationRead = 0;
        long durationWrite = 0;
        long durationConvert = 0;
    }

    public static long getDurationRead() {
        return durationRead;
    }

    public static long getDurationWrite() {
        return durationWrite;
    }

    public static long getDurationConvert() {
        return durationConvert;
    }

    public static void printDurations() {
        java.io.PrintStream out = System.out;
        out.println("Read duration: " + getTimeStringNano(durationRead));
        out.println("Write duration: " + getTimeStringNano(durationWrite));
        out.println("Convert duration: " + getTimeStringNano(durationConvert));
    }

    public static void writeBytes(RandomAccessFile file, byte[] bytes, long seek) throws IOException {
        file.seek(seek);
        writeBytes(file, bytes);
    }

    public static void writeBytes(RandomAccessFile file, byte[] bytes) throws IOException {
        long timeStart = System.nanoTime();
        file.write(bytes);
        long timeFinish = System.nanoTime();
        durationWrite += (timeFinish - timeStart);
    }

    public static void readBytes(RandomAccessFile file, byte[] bytes, int off, int len) throws IOException {
        long timeStart = System.nanoTime();
        file.read(bytes, off, len);
        long timeFinish = System.nanoTime();
        durationRead += (timeFinish - timeStart);
    }

    public static void readBytes(RandomAccessFile file, byte[] bytes) throws IOException {
        long timeStart = System.nanoTime();
        file.read(bytes);
        long timeFinish = System.nanoTime();
        durationRead += (timeFinish - timeStart);
    }

    private static void floatsToBytes(ByteBuffer buffer, float[] data, int off, int amount) {
        long timeStart = System.nanoTime();
        for (int i = 0; i < amount; i++) {
            buffer.putFloat(i * elementSizeInBytes, data[i + off]);
        }
        long timeFinish = System.nanoTime();
        durationConvert += (timeFinish - timeStart);
    }

    private static void bytesToFloats(ByteBuffer buffer, float[] data, int amount) {
        bytesToFloats(buffer, data, 0, amount);
    }

    private static void bytesToFloats(ByteBuffer buffer, float[] data, int off, int amount) {
        long timeStart = System.nanoTime();
        for (int i = 0; i < amount; i++) {
            data[i + off] = buffer.getFloat(i * elementSizeInBytes);
        }
        long timeFinish = System.nanoTime();
        durationConvert += (timeFinish - timeStart);
    }

    public static void writeFloats(RandomAccessFile file, float[] data, long seek) throws IOException {
        file.seek(seek);
        writeFloats(file, data);
    }

    public static void writeFloats(RandomAccessFile file, float[] data) throws IOException {
        writeFloatsPartial(file, data, data.length);
    }

    public static void writeFloatsPartial(RandomAccessFile file, float[] data, int amount) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(amount * elementSizeInBytes);
        floatsToBytes(buffer, data, 0, amount);
        writeBytes(file, buffer.array());
    }

    public static void writeFloatsMemLimit(RandomAccessFile file, float[] data, long seek) throws IOException {
        file.seek(seek);
        writeFloatsMemLimit(file, data);
    }

    public static void writeFloatsMemLimit(RandomAccessFile file, float[] data) throws IOException {
        writeFloatsPartialMemLimit(file, data, data.length);
    }

    public static void writeFloatsPartialMemLimit(RandomAccessFile file, float[] data, int amount) throws IOException {
        int chunksNumber = amount / memLimitBufferSize;
        int leftOver = amount % memLimitBufferSize;

        int bufferSize = Integer.min(amount, memLimitBufferSize);

        ByteBuffer buffer;
        if (chunksNumber > 0) {
            buffer = ByteBuffer.allocate(bufferSize * elementSizeInBytes);
            for (int i = 0; i < chunksNumber; i++) {
                floatsToBytes(buffer, data, i * bufferSize, bufferSize);
                writeBytes(file, buffer.array());
            }
        }
        if (leftOver > 0) {
            buffer = ByteBuffer.allocate(leftOver * elementSizeInBytes);
            floatsToBytes(buffer, data, amount - leftOver, leftOver);
            writeBytes(file, buffer.array());
        }
    }

    public static void readFloatsToBufferMemLimit(RandomAccessFile file, float[] data, long seek, int amount, int off) throws IOException {
        int chunksNumber = amount / memLimitBufferSize;
        int leftOver = amount % memLimitBufferSize;

        int bufferSize = Integer.min(amount, memLimitBufferSize);

        ByteBuffer buffer;
        if (chunksNumber > 0) {
            buffer = ByteBuffer.allocate(bufferSize * elementSizeInBytes);
            for (int i = 0; i < chunksNumber; i++) {
                readBytes(file, buffer.array());
                bytesToFloats(buffer, data, off + i * bufferSize, bufferSize);
            }
        }
        if (leftOver > 0) {
            buffer = ByteBuffer.allocate(leftOver * elementSizeInBytes);
            readBytes(file, buffer.array());
            bytesToFloats(buffer, data, off + amount - leftOver, leftOver);
        }
    }

    public static void readFloatsToBuffer(RandomAccessFile file, float[] data, long seek, int amount, int off) throws IOException {
        file.seek(seek);
        byte[] buffer = new byte[amount * elementSizeInBytes];
        readBytes(file, buffer);
        ByteBuffer bb = ByteBuffer.wrap(buffer);
        bytesToFloats(bb, data, off, amount);
    }

    /**
     * Чтение данных из файла начиная с указанной позиции.
     *
     * @param file   файл, из которого производится чтение.
     * @param seek   начальная позиция внутри файла.
     * @param amount количество читаемых float.
     * @return массив float.
     * @throws IOException возможное исключение.
     */
    public static float[] readFloats(RandomAccessFile file, long seek, int amount) throws IOException {
        file.seek(seek);
        return readFloats(file, amount);
    }

    /**
     * Чтение данных из файла начиная с текущей позиции.
     *
     * @param file   файл, из которого производится чтение.
     * @param amount количество читаемых float.
     * @return массив float.
     * @throws IOException возможное исключение.
     */
    public static float[] readFloats(RandomAccessFile file, int amount) throws IOException {
        float[] data = new float[amount];
        byte[] buffer = new byte[amount * elementSizeInBytes];
        readBytes(file, buffer);
        ByteBuffer bb = ByteBuffer.wrap(buffer);
        bytesToFloats(bb, data, amount);
        return data;
    }

    /**
     * Запись в файл массива char.
     * <p>
     * См. writeCharsPartial
     *
     * @param file файл, в который производится запись.
     * @param data массив данных.
     * @throws IOException возможное исключение.
     */
    public static void writeChars(RandomAccessFile file, char[] data) throws IOException {
        writeCharsPartial(file, data, data.length);
    }

    /**
     * Запись в файл части элементов массива char. При этом считаем, что имеем дело с 8-битной кодировкой.
     *
     * @param file   файл, в который производится запись.
     * @param data   массив данных.
     * @param amount количество элементов массива, которое следует записать в файл.
     * @throws IOException возможное исключение.
     */
    public static void writeCharsPartial(RandomAccessFile file, char[] data, int amount) throws IOException {
        int sizeOfChar = 1;
        ByteBuffer buffer = ByteBuffer.allocate(amount * sizeOfChar);
        for (int i = 0; i < amount; i++) {
            buffer.put(i * sizeOfChar, (byte) data[i]);
        }
        writeBytes(file, buffer.array());
    }
}
