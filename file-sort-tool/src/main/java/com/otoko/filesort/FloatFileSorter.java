package com.otoko.filesort;

import com.otoko.filesort.exception.FileNotFoundSortException;
import com.otoko.filesort.exception.InterruptedSortException;
import com.otoko.filesort.exception.SortException;
import com.otoko.filesort.reader.FloatBlockReader;
import com.otoko.filesort.task.SortTask;
import com.otoko.filesort.tree.SearchTreeAVLkvFloat;
import com.otoko.filesort.tree.SearchStructure;
import com.otoko.filesort.tree.SearchList;
import com.otoko.filesort.util.FileUtils;
import com.otoko.filesort.util.MemoryUtils;
import com.otoko.filesort.writer.FloatFileWriter;
import com.otoko.filesort.writer.ParallelFloatFileWriter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.*;

import static com.otoko.filesort.util.TimeUtils.getTimeString;

/**
 * Сортировщик файлов FLOAT-ов.
 */
public class FloatFileSorter {

    private final Object fileInLock = new Object();

    private ExecutorService exec;
    private CompletionService<Integer> compService;

    private void doFirstStage(LinkedList<FloatBlockReader> blocksAsList,
                              RandomAccessFile fileIn, RandomAccessFile fileOut)
            throws IOException, ExecutionException, InterruptedException {

        long fileLength = fileIn.length();

        //calculate block size
        int blockSize = MemoryUtils.getMaxBlockSizeTotal();
        long dataLength = fileLength / FileUtils.elementSizeInBytes;
        int blockNumber = Math.toIntExact(dataLength / blockSize);
        int leftOver = Math.toIntExact(dataLength % blockSize);

        int processors = Runtime.getRuntime().availableProcessors();
        //System.out.println("Processors: " + processors);
        int subBlockSize = Integer.min(MemoryUtils.getMaxBlockSizePerCore(), blockSize / processors);
        exec = Executors.newFixedThreadPool(processors);
        compService = new ExecutorCompletionService<>(exec);

        long taskPos;
        int taskSize;
        for (long i = 0; i < blockNumber; i++) { // must be long type
            taskPos = i * blockSize;
            taskSize = blockSize;
            workWithBlock(blocksAsList, fileIn, fileOut, taskPos, taskSize, subBlockSize);
        }
        if (leftOver > 0) {
            taskPos = dataLength - leftOver;
            taskSize = leftOver;
            subBlockSize = Integer.min(subBlockSize, leftOver / processors);
            workWithBlock(blocksAsList, fileIn, fileOut, taskPos, taskSize, subBlockSize);
        }

        exec.shutdown();
    }

    private int workWithSubBlock(float[] data, RandomAccessFile fileIn, long pos, int size, int off) throws IOException {

        synchronized (fileInLock) {
            FileUtils.readFloatsToBufferMemLimit(fileIn, data, pos * FileUtils.elementSizeInBytes, size, off);
        }
        Arrays.sort(data, off, off + size);
        return 0;
    }

    // read, sort, write sorted block of data
    private void workWithBlock(Collection<FloatBlockReader> blockReaders, RandomAccessFile fileIn,
                               RandomAccessFile fileOut, long pos, int size, int subBlockSize)
            throws IOException, ExecutionException, InterruptedException {

        int tasksCounter = 0;

        float[] data = new float[size];

        int subBlockNumber = Math.toIntExact(size / subBlockSize);
        int subLeftOver = Math.toIntExact(size % subBlockSize);

        // add tasks
        for (int i = 0; i < subBlockNumber; i++) {
            int taskOff = i * subBlockSize;
            long taskPos = pos + taskOff;
            compService.submit(() -> {
                //System.out.println("Thread: " + Thread.currentThread().getName());
                return workWithSubBlock(data, fileIn, taskPos, subBlockSize, taskOff);
            });
            tasksCounter++;
        }
        if (subLeftOver > 0) {
            int taskOff = size - subLeftOver;
            long taskPos = pos + taskOff;
            compService.submit(() -> {
                //System.out.println("Thread: " + Thread.currentThread().getName());
                return workWithSubBlock(data, fileIn, taskPos, subLeftOver, taskOff);
            });
            tasksCounter++;
        }

        // wait for tasks
        while (tasksCounter > 0) {
            Future<Integer> resultFuture = compService.take();
            resultFuture.get();
            tasksCounter--;
        }

        if (subBlockNumber > 0) {
            Arrays.sort(data);
        }
        long newPos = fileOut.length() / FileUtils.elementSizeInBytes;
        // ..and writes to temp file
        FileUtils.writeFloatsMemLimit(fileOut, data);

        // 2nd stage will read from temp file
        FloatBlockReader blockReader = new FloatBlockReader(
                fileOut, data[0], newPos, newPos + size);
        blockReaders.add(blockReader);
    }

    private void doSecondStage(SearchStructure blocks, RandomAccessFile fileOut) throws IOException {
        FloatFileWriter writer = new ParallelFloatFileWriter(fileOut);
        //File f = new File("file.txt");
        //FloatFileWriter writer = new SimpleBufferedFloatFileWriter(f);

        FloatBlockReader block;
        FloatBlockReader nextBlock;
        float value;

        while (!blocks.isEmpty()) {
            block = blocks.pollFirst();
            value = block.getFloatValue();

            if (!blocks.isEmpty()) {
                nextBlock = blocks.first();
                while (true) {
                    writer.writeFloat(value);
                    if (!block.hasNext()) {
                        break;
                    }
                    value = block.readNext();
                    if (value > nextBlock.getFloatValue()) {
                        blocks.add(block);
                        break;
                    }
                }
            } else {
                while (true) {
                    writer.writeFloat(value);
                    if (!block.hasNext()) {
                        break;
                    }
                    value = block.readNext();
                }
            }
        }

        writer.close();
    }

    /**
     * Команда на сортировку.
     *
     * @param sortTask Задача сортировки.
     * @throws SortException Возможное исключение в процессе сортировки.
     */
    public void sortFile(SortTask sortTask) throws SortException {
        String fileName = sortTask.getFileName();
        String tempFileName = sortTask.getTempFileName();

        Thread statusUpdater = null;

        try {
            RandomAccessFile fileIn = new RandomAccessFile(fileName, "rw");
            RandomAccessFile fileOut = new RandomAccessFile(tempFileName, "rw");

            // reset inner cursor
            fileOut.setLength(0);

            long fileLength = fileIn.length();

            if (fileLength == 0) {
                throw new SortException("Empty file");
                //System.out.println("Empty file");
                //return;
            }
            if (fileLength % FileUtils.elementSizeInBytes > 0) {
                throw new SortException("Wrong file size");
                //System.out.println("wrong file size");
                //return;
            }
            MemoryUtils.calculateValues(fileLength);
            FileUtils.setMemLimitBufferSize(MemoryUtils.getMemLimitBufferSize());

            sortTask.setStatus("1st stage");

            long timeStart1stStage = System.currentTimeMillis();

            LinkedList<FloatBlockReader> blocksAsList = new LinkedList<>();

            statusUpdater = new Thread(() -> {
                try {
                    while (true) {
                        long curLength = fileOut.length();
                        double percent = curLength / (fileLength / 100.0);
                        sortTask.setStatus("1st stage " + String.format("%.2f", percent) + "%");
                        Thread.sleep(1000);
                    }
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace(); //TODO
                }
            });
            statusUpdater.start();

            //MemoryUtils.printLimits();

            // read from the original and write to temp file
            doFirstStage(blocksAsList, fileIn, fileOut);

            statusUpdater.stop();

            long timeFinish1stStage = System.currentTimeMillis();

            System.out.println("1st stage duration: "
                    + getTimeString(timeFinish1stStage - timeStart1stStage));

            System.out.println("1st stage blocks: " + blocksAsList.size());

            sortTask.setStatus("2nd stage");

            long timeStart2ndStage = System.currentTimeMillis();

            SearchStructure blocks;
            if (blocksAsList.size() < 300) {
                blocks = new SearchList(blocksAsList);
            } else {
                blocks = new SearchTreeAVLkvFloat(blocksAsList);
            }

            // reset inner cursor
            fileIn.setLength(0);

            statusUpdater = new Thread(() -> {
                try {
                    while (true) {
                        long curLength = fileIn.length();
                        double percent = curLength / (fileLength / 100.0);
                        sortTask.setStatus("2nd stage " + String.format("%.2f", percent) + "%");
                        Thread.sleep(1000);
                    }
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace(); //TODO
                }
            });
            statusUpdater.start();

            //MemoryUtils.printLimits();

            // write over the original file
            doSecondStage(blocks, fileIn);

            statusUpdater.stop();

            long timeFinish2ndStage = System.currentTimeMillis();

            System.out.println("2nd stage duration: "
                    + getTimeString(timeFinish2ndStage - timeStart2ndStage));

            fileOut.close();
            fileIn.close();

            sortTask.setStatus("Finished");
            sortTask.finish();
        } catch (FileNotFoundException e) {
            if (statusUpdater != null && statusUpdater.isAlive()) {
                statusUpdater.stop();
            }
            throw new FileNotFoundSortException("Could not found file", e);
        } catch (IOException e) {
            if (statusUpdater != null && statusUpdater.isAlive()) {
                statusUpdater.stop();
            }
            throw new SortException("Could not alter file", e);
        } catch (InterruptedException e) {
            if (statusUpdater.isAlive()) {
                statusUpdater.stop();
            }
            exec.shutdown();
            throw new InterruptedSortException("Execution of separate thread was interrupted", e);
        } catch (ExecutionException e) {
            if (statusUpdater.isAlive()) {
                statusUpdater.stop();
            }
            exec.shutdown();
            throw new InterruptedSortException("Error during execution of separate thread", e);
        }
    }
}
