package com.otoko.filesort.tree;

import com.otoko.filesort.reader.FloatBlockReader;

import java.util.Collections;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Реализация структуры поиска через список списков.
 */
public class SearchMultiList implements SearchStructure {

    LinkedList<LinkedList<FloatBlockReader>> multiList;

    public SearchMultiList(LinkedList<FloatBlockReader> list) {
        Collections.sort(list);

        multiList = new LinkedList<>();

        float minValue = list.getFirst().getFloatValue();

        LinkedList<FloatBlockReader> subList = new LinkedList<>();
        for (FloatBlockReader reader : list) {
            if (reader.getFloatValue() > minValue) {
                multiList.add(subList); // minValue

                minValue = reader.getFloatValue();
                subList = new LinkedList<>();
            }
            subList.add(reader);
        }
        if (!subList.isEmpty()) {
            multiList.add(subList); // minValue
        }
    }

    @Override
    public FloatBlockReader pollFirst() {
        LinkedList<FloatBlockReader> subList = multiList.getFirst();
        FloatBlockReader reader = subList.pollFirst();
        if (subList.size() == 0) {
            multiList.removeFirst();
        }
        return reader;
    }

    @Override
    public void add(FloatBlockReader obj) {
        float floatValue = obj.getFloatValue();
        ListIterator<LinkedList<FloatBlockReader>> iter = multiList.listIterator();
        LinkedList<FloatBlockReader> subList;
        float subListFloat;
        while (iter.hasNext()) {
            subList = iter.next();
            subListFloat = subList.getFirst().getFloatValue();
            if (subListFloat == floatValue) {
                subList.add(obj);
                return;
            } else if (subListFloat > floatValue) {
                iter.previous();
                subList = new LinkedList<>();
                subList.add(obj);
                iter.add(subList);
                return;
            }
        }
        subList = new LinkedList<>();
        subList.add(obj);
        iter.add(subList);
    }

    @Override
    public boolean isEmpty() {
        return multiList.isEmpty();
    }

    @Override
    public FloatBlockReader first() {
        return multiList.getFirst().getFirst();
    }
}
