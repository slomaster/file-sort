package com.otoko.filesort.tree;

import com.otoko.filesort.reader.FloatBlockReader;

import java.util.Collections;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Реализация структуры поиска через список.
 */
public class SearchList implements SearchStructure {

    LinkedList<FloatBlockReader> list;

    public SearchList(LinkedList<FloatBlockReader> list) {
        Collections.sort(list);
        this.list = list;
    }

    @Override
    public FloatBlockReader pollFirst() {
        return list.pollFirst();
    }

    @Override
    public void add(FloatBlockReader obj) {
        float floatValue = obj.getFloatValue();
        ListIterator<FloatBlockReader> iter = list.listIterator();
        while (iter.hasNext()) {
            if (iter.next().getFloatValue() >= floatValue) {
                iter.previous();
                break;
            }
        }
        iter.add(obj);
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public FloatBlockReader first() {
        return list.getFirst();
    }
}
