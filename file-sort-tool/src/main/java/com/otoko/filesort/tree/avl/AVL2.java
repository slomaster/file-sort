package com.otoko.filesort.tree.avl;

import com.otoko.filesort.reader.FloatBlockReader;

import java.util.LinkedList;

/**
 * Реализация AVL-дерева, где
 * тип ключа - float
 * тип значения - список FloatBlockReader'ов.
 */
public class AVL2 {

    public class Node {
        private int h;
        private int balance;
        float key;
        LinkedList<FloatBlockReader> value;
        private Node left, right, father;

        public Node(float key, LinkedList<FloatBlockReader> value, Node father) {
            this.key = key;
            this.value = value;
            this.left = this.right = null;
            this.father = father;
            this.h = 1;
            this.balance = 0;
        }

        public Node next() {
            return getHigherNode(this.key);
        }

        public Node previous() {
            return getLowerNode(this.key);
        }

        public float getKey() {
            return this.key;
        }

        public LinkedList<FloatBlockReader> getValue() {
            return this.value;
        }
    }

    private Node root;

    private Node getHigherNode(float key) {
        Node p = root;
        while (p != null) {
            int cmp = Float.compare(key, p.key);
            if (cmp < 0) {
                if (p.left != null)
                    p = p.left;
                else
                    return p;
            } else {
                if (p.right != null) {
                    p = p.right;
                } else {
                    Node father = p.father;
                    Node ch = p;
                    while (father != null && ch == father.right) {
                        ch = father;
                        father = father.father;
                    }
                    return father;
                }
            }
        }
        return null;
    }

    private Node getLowerNode(float key) {
        Node p = root;
        while (p != null) {
            int cmp = Float.compare(key, p.key);
            if (cmp > 0) {
                if (p.right != null)
                    p = p.right;
                else
                    return p;
            } else {
                if (p.left != null) {
                    p = p.left;
                } else {
                    Node father = p.father;
                    Node ch = p;
                    while (father != null && ch == father.left) {
                        ch = father;
                        father = father.father;
                    }
                    return father;
                }
            }
        }
        return null;
    }

    public Node getFirstNode() {
        return min(root);
    }

    public Node getLastNode() {
        return max(root);
    }

    private int height(Node x, Node y) {
        if (x == null && y == null) return 0;
        else if (x == null) return y.h;
        else if (y == null) return x.h;
        else return Math.max(x.h, y.h);
    }

    private int balance(Node x, Node y) {
        if (x == null && y == null) return 0;
        else if (x == null) return -y.h;
        else if (y == null) return x.h;
        else return x.h - y.h;
    }

    private Node add(Node node, float key, LinkedList<FloatBlockReader> value, Node father) {
        if (node == null) {
            return new Node(key, value, father);
        }
        int compareResult = Float.compare(key, node.key);
        if (compareResult > 0) {
            node.right = add(node.right, key, value, node);
            node.h = height(node.left, node.right) + 1;
        } else if (compareResult < 0) {
            node.left = add(node.left, key, value, node);
            node.h = height(node.left, node.right) + 1;
        } else {
            node.value = value;
        }
        node.balance = balance(node.left, node.right);
        if (node.balance == -2) {
            node = leftRotation(node);
        } else if (node.balance == 2) {
            node = rightRotation(node);
        }
        return node;
    }

    private Node leftRotation(Node node) {
        if (node.right.right == null && node.right.left != null) {
            node.right = rightRotation(node.right);
            node = leftRotation(node);
        } else if (node.right.left == null || node.right.left.h <= node.right.right.h) {
            Node newNode = node.right;
            newNode.father = node.father;
            node.right = newNode.left;
            if (node.right != null)
                node.right.father = node;
            node.h = height(node.left, node.right) + 1;
            node.father = newNode;
            node.balance = balance(node.left, node.right);
            newNode.left = node;
            node = newNode;
            node.balance = balance(node.left, node.right);
            node.h = height(node.left, node.right) + 1;
        } else {
            node.right = rightRotation(node.right);
            node = leftRotation(node);
        }
        return node;
    }

    private Node rightRotation(Node node) {
        if (node.left.right != null && node.left.left == null) {
            node.left = leftRotation(node.left);
            node = rightRotation(node);
        } else if (node.left.right == null || node.left.right.h <= node.left.left.h) {
            Node newNode = node.left;
            newNode.father = node.father;
            node.left = newNode.right;
            if (node.left != null)
                node.left.father = node;
            node.h = height(node.left, node.right) + 1;
            node.father = newNode;
            node.balance = balance(node.left, node.right);
            newNode.right = node;
            node = newNode;
            node.balance = balance(node.left, node.right);
            node.h = height(node.left, node.right) + 1;
        } else {
            node.left = leftRotation(node.left);
            node = rightRotation(node);
        }
        return node;
    }

    public void add(float key, LinkedList<FloatBlockReader> value) {
        root = add(root, key, value, null);
    }

    public void addElement(FloatBlockReader reader) {
        root = addElement(root, reader.getFloatValue(), reader, null);
    }

    private Node addElement(Node node, float key, FloatBlockReader reader, Node father) {
        if (node == null) {
            LinkedList<FloatBlockReader> newList = new LinkedList<>();
            newList.add(reader);
            return new Node(key, newList, father);
        }
        int compareResult = Float.compare(key, node.key);
        if (compareResult > 0) {
            node.right = addElement(node.right, key, reader, node);
            node.h = height(node.left, node.right) + 1;
        } else if (compareResult < 0) {
            node.left = addElement(node.left, key, reader, node);
            node.h = height(node.left, node.right) + 1;
        } else {
            node.value.add(reader);
        }
        node.balance = balance(node.left, node.right);
        if (node.balance == -2) {
            node = leftRotation(node);
        } else if (node.balance == 2) {
            node = rightRotation(node);
        }
        return node;
    }

    private Node delete(Node node, float key) {
        if (node == null) return null;
        int compareResult = Float.compare(key, node.key);
        if (compareResult > 0) {
            node.right = delete(node.right, key);
        } else if (compareResult < 0) {
            node.left = delete(node.left, key);
        } else {
            if (node.right == null && node.left == null) {
                node = null;
            } else if (node.right == null) {
                node.left.father = node.father;
                node = node.left;
            } else if (node.left == null) {
                node.right.father = node.father;
                node = node.right;
            } else {
                if (node.right.left == null) {
                    node.right.left = node.left;
                    node.right.father = node.father;
                    node.left.father = node.right;
                    node = node.right;
                } else {
                    Node res = min(node.right);
                    node.key = res.key;
                    node.value = res.value;
                    delete(node.right, node.key);
                }
            }
        }
        if (node != null) {
            node.h = height(node.left, node.right) + 1;
            node.balance = balance(node.left, node.right);
            if (node.balance == -2) {
                node = leftRotation(node);
            } else if (node.balance == 2) {
                node = rightRotation(node);
            }
        }
        return node;
    }

    //fast and ugly delete for min node
    public void deleteMinNode(Node node) {
        if (node.father != null) {
            node.father.left = delete(node, node.key);
        } else {
            delete(node.key);
        }
    }

    public void delete(float key) {
        root = delete(root, key);
    }

    public float minKey() {
        return min(root).key;
    }

    public float maxKey() {
        return max(root).key;
    }

    private Node min(Node node) {
        if (node.left == null) return node;
        return min(node.left);
    }

    private Node max(Node node) {
        if (node.right == null) return node;
        return min(node.right);
    }

    private LinkedList<FloatBlockReader> get(Node node, float key) {
        if (node == null) return null;
        int compareResult = Float.compare(key, node.key);
        if (compareResult == 0) {
            return node.value;
        } else if (compareResult > 0) {
            return get(node.right, key);
        } else {
            return get(node.left, key);
        }
    }

    public LinkedList<FloatBlockReader> get(float key) {
        return get(root, key);
    }

    private void print(Node node, int level) {
        if (node != null) {
            print(node.right, level + 1);
            for (int i = 0; i < level; i++) {
                System.out.print("\t");
            }
            System.out.println(node.key + "->" + node.value + " h=" + node.h + " balance=" + node.balance);
            print(node.left, level + 1);
        }
    }

    public void print() {
        print(root, 0);
    }

    public boolean isEmpty() {
        return (root == null);
    }

}
