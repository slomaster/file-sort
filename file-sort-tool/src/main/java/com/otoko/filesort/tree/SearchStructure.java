package com.otoko.filesort.tree;

import com.otoko.filesort.reader.FloatBlockReader;

/**
 * Структура поиска. Внутри себя отсортирована.
 */
public interface SearchStructure {

    /**
     * Извлечь первый reader (получить с удалением его из структуры).
     *
     * @return Первый reader.
     */
    FloatBlockReader pollFirst();

    /**
     * Добавить reader.
     *
     * @param obj Reader.
     */
    void add(FloatBlockReader obj);

    /**
     * Проверка на отсутствие reader'ов в структуре.
     *
     * @return {@code true} если пусто, иначе {@code false}.
     */
    boolean isEmpty();

    /**
     * Получить первый reader.
     *
     * @return Первый reader.
     */
    FloatBlockReader first();
}
