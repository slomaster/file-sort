package com.otoko.filesort.tree;

import com.otoko.filesort.reader.FloatBlockReader;
import com.otoko.filesort.tree.avl.AVL2;

import java.util.Collections;
import java.util.LinkedList;

/**
 * Реализация структуры поиска через AVL-дерево (AVL2-вариант).
 */
public class SearchTreeAVL2 implements SearchStructure {

    private final AVL2 avl;
    private AVL2.Node minNode;

    public SearchTreeAVL2(LinkedList<FloatBlockReader> list) {
        Collections.sort(list);

        avl = new AVL2();

        float minValue = list.getFirst().getFloatValue();

        LinkedList<FloatBlockReader> subList = new LinkedList<>();
        for (FloatBlockReader reader : list) {
            if (reader.getFloatValue() > minValue) {
                avl.add(minValue, subList);

                minValue = reader.getFloatValue();
                subList = new LinkedList<>();
            }
            subList.add(reader);
        }
        if (!subList.isEmpty()) {
            avl.add(minValue, subList);
        }

        minNode = avl.getFirstNode();
    }

    @Override
    public FloatBlockReader pollFirst() {
        LinkedList<FloatBlockReader> blockReaders = minNode.getValue();

        FloatBlockReader reader = blockReaders.pollFirst();
        if (blockReaders.size() == 0) {
            //float key = minNode.getKey();
            AVL2.Node delNode = minNode;
            minNode = minNode.next();
            avl.deleteMinNode(delNode);
            //avl.delete(key);
        }
        return reader;
    }

    @Override
    public void add(FloatBlockReader reader) {
        avl.addElement(reader);
    }

    @Override
    public boolean isEmpty() {
        return avl.isEmpty();
    }

    @Override
    public FloatBlockReader first() {
        return minNode.getValue().getFirst();
    }
}