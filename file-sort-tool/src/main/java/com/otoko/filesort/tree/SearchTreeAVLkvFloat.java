package com.otoko.filesort.tree;

import com.otoko.filesort.reader.FloatBlockReader;
import com.otoko.filesort.tree.avl.AVLkvFloat;

import java.util.Collections;
import java.util.LinkedList;

/**
 * Реализация структуры поиска через AVL-дерево (AVLkvFloat-вариант).
 */
public class SearchTreeAVLkvFloat implements SearchStructure {

    private final AVLkvFloat<LinkedList<FloatBlockReader>> avl;
    private AVLkvFloat<LinkedList<FloatBlockReader>>.Node minNode;

    public SearchTreeAVLkvFloat(LinkedList<FloatBlockReader> list) {
        Collections.sort(list);

        avl = new AVLkvFloat<>();

        float minValue = list.getFirst().getFloatValue();

        LinkedList<FloatBlockReader> subList = new LinkedList<>();
        for (FloatBlockReader reader : list) {
            if (reader.getFloatValue() > minValue) {
                avl.add(minValue, subList);

                minValue = reader.getFloatValue();
                subList = new LinkedList<>();
            }
            subList.add(reader);
        }
        if (!subList.isEmpty()) {
            avl.add(minValue, subList);
        }

        minNode = avl.getFirstNode();
    }

    @Override
    public FloatBlockReader pollFirst() {
        LinkedList<FloatBlockReader> blockReaders = minNode.getValue();

        FloatBlockReader reader = blockReaders.pollFirst();
        if (blockReaders.size() == 0) {
            //float key = minNode.getKey();
            AVLkvFloat<LinkedList<FloatBlockReader>>.Node delNode = minNode;
            minNode = minNode.next();
            avl.deleteMinNode(delNode);
            //avl.delete(key);
        }
        return reader;
    }

    @Override
    public void add(FloatBlockReader reader) {
        float floatValue = reader.getFloatValue();
        LinkedList<FloatBlockReader> linkedList = avl.get(floatValue);
        if (linkedList == null) {
            linkedList = new LinkedList<>();
            linkedList.add(reader);
            avl.add(floatValue, linkedList);
        } else {
            linkedList.add(reader);
        }
    }

    @Override
    public boolean isEmpty() {
        return avl.isEmpty();
    }

    @Override
    public FloatBlockReader first() {
        return minNode.getValue().getFirst();
    }
}