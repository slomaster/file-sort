package com.otoko.filesort;

import com.otoko.filesort.exception.SortException;
import com.otoko.filesort.task.SortTask;
import com.otoko.filesort.task.SortTaskListener;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.*;

/**
 * Многозадачный менеджер сортировки файлов FLOAT-ов.
 *
 * В текущей реализации ставит задачи в очередь не заботясь о доступных в текущий момент ресурсах, каждая следующая
 * задача поступает в обработку не дожидаясь завершения предыдущих.
 * TODO устранить этот недостаток
 */
public class FloatFileSortMultiTaskManager implements SortTaskListener {

    private final ExecutorService executor;
    private final CompletionService<Integer> completionService;
    private final HashMap<String, SortTask> tasks;

    private int tasksCounter = 0;

    public FloatFileSortMultiTaskManager() {
        executor = Executors.newSingleThreadExecutor();
        completionService = new ExecutorCompletionService<>(executor);
        tasks = new HashMap<>();
    }

    public SortTask getTask(String fileName) {
        return tasks.get(fileName);
    }

    private static void performTask(SortTask task) {
        task.setStatus("In process...");
        FloatFileSorter fileSorter = new FloatFileSorter();
        try {
            fileSorter.sortFile(task);
        } catch (SortException e) {
            task.setStatus("Error: " + e.getMessage());
        }
    }

    public void sortFile(String fileName, String tempFileName)  {
        SortTask task = new SortTask(fileName, tempFileName);
        task.addListener(this);
        File file = new File(fileName);
        tasks.put(file.getName(), task);
        completionService.submit(() -> {
            performTask(task);
            return 0;
        });
        tasksCounter++;
    }

    public void shutdown() {
        try {
            while (tasksCounter > 0) {
                Future<Integer> resultFuture = completionService.take();
                resultFuture.get();
                tasksCounter--;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace(); // TODO
        } finally {
            executor.shutdown();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        shutdown();
        super.finalize();
    }

    @Override
    public void taskFinished(SortTask sortTask) {
        sortTask.setStatus("Complete");

        try {
            Files.delete(Paths.get(sortTask.getTempFileName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}