package com.otoko.filesort.exception;

public class InterruptedSortException extends SortException {

    public InterruptedSortException(String message) { super(message); }

    public InterruptedSortException(String message, Throwable cause) {
        super(message, cause);
    }
}
