package com.otoko.filesort.exception;

public class FileNotFoundSortException extends SortException {

    public FileNotFoundSortException(String message) { super(message); }

    public FileNotFoundSortException(String message, Throwable cause) {
        super(message, cause);
    }
}
