package com.otoko.filesort.exception;

public class SortException extends Exception {

    public SortException(String message) {
        super(message);
    }

    public SortException(String message, Throwable cause) {
        super(message, cause);
    }
}
