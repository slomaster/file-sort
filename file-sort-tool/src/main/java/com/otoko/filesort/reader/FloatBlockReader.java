package com.otoko.filesort.reader;

import com.otoko.filesort.util.FileUtils;
import com.otoko.filesort.util.MemoryUtils;

import java.io.IOException;
import java.io.RandomAccessFile;

import static com.otoko.filesort.util.FileUtils.elementSizeInBytes;

/**
 * Специализированный reader для блока файла с FLOAT-ами.
 */
public class FloatBlockReader implements Comparable<FloatBlockReader> {

    private final RandomAccessFile file;

    private float floatValue;
    private long curIndex;
    private final long endIndex;

    private float[] buffer;
    private int bufferSize;
    private int bufferCursor;

    /**
     * Конструктор.
     *
     * Поскольку значение первого элемента блока уже известно, то оно указывается как исходное floatValue и первый
     * элемент уже не обязательно читать из файла.
     *
     * @param file Файл, из которого производится чтение.
     * @param floatValue Начальное значение floatValue.
     * @param startIndex Начальная позиция внутри файла.
     * @param endIndex Конечная позиция внутри файла.
     * @param bufferSize Размер буфера.
     */
    public FloatBlockReader(RandomAccessFile file, float floatValue, long startIndex, long endIndex, int bufferSize) {
        this.file = file;
        this.endIndex = endIndex;
        this.curIndex = startIndex + 1; // assume we've read one element
        this.floatValue = floatValue;
        this.bufferSize = Integer.min((int)(endIndex - startIndex),
                Integer.min(bufferSize, MemoryUtils.getMaxReadBufferSize()));
        buffer = null;
    }

    /**
     * Конструктор. Размер буфера устанавливается значением по-умолчанию.
     *
     * @param file Файл, из которого производится чтение.
     * @param floatValue Начальное значение floatValue.
     * @param startIndex Начальная позиция внутри файла.
     * @param endIndex Конечная позиция внутри файла.
     */
    public FloatBlockReader(RandomAccessFile file, float floatValue, long startIndex, long endIndex) {
        this(file, floatValue, startIndex, endIndex, MemoryUtils.getMaxReadBufferSize());
    }

    /**
     * Чтение находящегося в текущей позиции FLOAT-а, внутренний указатель позиции при этом сдвигается.
     *
     * @return Прочитанное значение.
     * @throws IOException Возможное исключение ошибки чтения.
     */
    public float readNext() throws IOException {
        floatValue = readFloatBuffered();
        curIndex++;
        return floatValue;
    }

    private void prefetchBuffer() throws IOException {
        buffer = FileUtils.readFloats(file, curIndex * elementSizeInBytes,
                Integer.min((int)(endIndex - curIndex), bufferSize));
        bufferSize = buffer.length;
        bufferCursor = 0;
    }

    private float readFloatBuffered() throws IOException {
        if (buffer == null || bufferCursor >= bufferSize) {
            prefetchBuffer();
            return readFloatBuffered();
        } else {
            return buffer[bufferCursor++];
        }
    }

    public float getFloatValue() {
        return floatValue;
    }

    public boolean hasNext() {
        return (curIndex < endIndex);
    }

    /**
     * Реализация метода из Comparable. Необходимо для того, чтобы использовать Collections.sort()
     *
     * @param o сравниваемый объект.
     * @return результат сравнения.
     */
    @Override
    public int compareTo(FloatBlockReader o) {
        return Float.compare(floatValue, o.floatValue);
    }
}