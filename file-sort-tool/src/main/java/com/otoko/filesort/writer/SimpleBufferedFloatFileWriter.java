package com.otoko.filesort.writer;

import com.otoko.filesort.util.MemoryUtils;

import java.io.*;
import java.nio.ByteBuffer;

/**
 * Специализированный writer в файл FLOAT-ов.
 *
 * Реализация через {@link java.io.BufferedOutputStream}.
 */
public class SimpleBufferedFloatFileWriter implements FloatFileWriter {

    private final FileOutputStream fileStream;
    private final BufferedOutputStream stream;
    private final ByteBuffer bb;

    public SimpleBufferedFloatFileWriter(File file, int bufferSize) throws FileNotFoundException {
        int bSize = Integer.min(bufferSize, MemoryUtils.getMaxWriteBufferSize());
        this.fileStream = new FileOutputStream(file);
        this.stream = new BufferedOutputStream(fileStream, bSize);
        this.bb = ByteBuffer.allocate(4);
    }

    public SimpleBufferedFloatFileWriter(File file) throws FileNotFoundException {
        this(file, MemoryUtils.getMaxWriteBufferSize());
    }

    public void writeFloat(float v) throws IOException {
        bb.putFloat(0, v);
        stream.write(bb.array());
    }

    public void close() throws IOException {
        stream.close();
        fileStream.close();
    }
}
