package com.otoko.filesort.writer;

import com.otoko.filesort.util.FileUtils;
import com.otoko.filesort.util.MemoryUtils;

import java.io.Closeable;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Специализированный writer для файла CHAR-ов.
 *
 * Написан для отладки.
 */
public class CharFileWriter implements Closeable {

    private final RandomAccessFile file;
    private final char[] buffer;
    private int bufferCursor;
    private final int bufferSize;

    public CharFileWriter(RandomAccessFile file, int bufferSize) {
        this.file = file;
        this.bufferSize = Integer.min(bufferSize, MemoryUtils.getMaxWriteBufferSize());
        bufferCursor = 0;
        buffer = new char[bufferSize];
    }

    public CharFileWriter(RandomAccessFile file) { this(file, MemoryUtils.getMaxWriteBufferSize()); }

    private void writeBufferToFile() throws IOException {
        FileUtils.writeChars(file, buffer);
        bufferCursor = 0;
    }

    public void writeChar(char v) throws IOException {
        if (bufferCursor >= bufferSize) {
            writeBufferToFile();
            writeChar(v);
        } else {
            buffer[bufferCursor++] = v;
        }
    }

    public void close() throws IOException {
        //force write buffer if not empty
        if (bufferCursor > 0)
            FileUtils.writeCharsPartial(file, buffer, bufferCursor);
    }
}
