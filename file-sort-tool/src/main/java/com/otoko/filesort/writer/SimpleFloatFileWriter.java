package com.otoko.filesort.writer;

import com.otoko.filesort.util.FileUtils;
import com.otoko.filesort.util.MemoryUtils;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Специализированный writer в файл FLOAT-ов.
 */
public class SimpleFloatFileWriter implements FloatFileWriter {

    private final RandomAccessFile file;
    private final float[] buffer;
    private int bufferCursor;
    private final int bufferSize;

    public SimpleFloatFileWriter(RandomAccessFile file, int bufferSize) {
        this.file = file;
        this.bufferSize = Integer.min(bufferSize, MemoryUtils.getMaxWriteBufferSize());
        bufferCursor = 0;
        buffer = new float[this.bufferSize];
    }

    public SimpleFloatFileWriter(RandomAccessFile file) {
        this(file, MemoryUtils.getMaxWriteBufferSize());
    }

    private void writeBufferToFile() throws IOException {
        FileUtils.writeFloats(file, buffer);
        bufferCursor = 0;
    }

    public void writeFloat(float v) throws IOException {
        if (bufferCursor >= bufferSize)
            writeBufferToFile();

        buffer[bufferCursor++] = v;
    }

    public void close() throws IOException {
        //force write buffer if not empty
        if (bufferCursor > 0)
            FileUtils.writeFloatsPartial(file, buffer, bufferCursor);
    }
}
