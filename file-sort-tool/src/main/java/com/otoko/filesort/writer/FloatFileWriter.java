package com.otoko.filesort.writer;

import java.io.Closeable;
import java.io.IOException;

/**
 * Интерфейс специализированного writer'а в файл FLOAT-ов.
 */
public interface FloatFileWriter extends Closeable {

    void writeFloat(float v) throws IOException;
}