package com.otoko.filesort.writer;

import com.otoko.filesort.util.FileUtils;
import com.otoko.filesort.util.MemoryUtils;

import java.io.RandomAccessFile;
import java.nio.FloatBuffer;
import java.util.concurrent.*;

/**
 * Специализированный writer в файл FLOAT-ов.
 *
 * Многопоточная реализация с двойной буферизацией. Один из буферов накапливает данные, пока из второго буфера
 * производится непосредственно запись на диск, затем они меняются.
 */
public class ParallelFloatFileWriter implements FloatFileWriter {

    private final RandomAccessFile file;
    private int bufferCursor;
    private final int bufferSize;

    private final FloatBuffer[] buffers;
    private int curBuffer;

    private final ExecutorService executor;
    private final CompletionService<Integer> completionService;
    private boolean isBusy;

    public ParallelFloatFileWriter(RandomAccessFile file, int bufferSize) {
        this.file = file;
        this.bufferSize = Integer.min(bufferSize, MemoryUtils.getMaxWriteBufferSize());
        bufferCursor = 0;

        buffers = new FloatBuffer[2];
        buffers[0] = FloatBuffer.allocate(this.bufferSize);
        buffers[1] = FloatBuffer.allocate(this.bufferSize);
        curBuffer = 0;

        executor = Executors.newSingleThreadExecutor();
        completionService = new ExecutorCompletionService<>(executor);
        isBusy = false;
    }

    public ParallelFloatFileWriter(RandomAccessFile file) {
        this(file, MemoryUtils.getMaxWriteBufferSize());
    }

    private void waitIfBusy() {
        if (isBusy) {
            try {
                Future<Integer> resultFuture = completionService.take();
                resultFuture.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace(); // TODO
            }
            isBusy = false;
        }
    }

    private void writeBufferToFile() {
        waitIfBusy();
        {
            int taskBuffer = curBuffer;
            int taskCursor = bufferCursor;
            completionService.submit(() -> {
                //System.out.println("Thread: " + Thread.currentThread().getName());
                FileUtils.writeFloatsPartial(file, buffers[taskBuffer].array(), taskCursor);
                return 0;
            });
        }
        isBusy = true;

        if (curBuffer == 0) {
            curBuffer = 1;
        } else {
            curBuffer = 0;
        }
        bufferCursor = 0;
    }

    public void writeFloat(float v) {
        if (bufferCursor >= bufferSize)
            writeBufferToFile();

        buffers[curBuffer].put(bufferCursor++, v);
    }

    public void close() {
        //force write buffer if not empty
        if (bufferCursor > 0)
            writeBufferToFile();
        //shutdown executor
        waitIfBusy();
        executor.shutdown();
    }
}
