package com.otoko.websort.controller;

import com.otoko.websort.service.exception.StorageFileNotFoundException;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

/**
 * Контроллер загрузки файлов.
 */
public interface FileUploadController {

    /**
     * Главная страница.
     *
     * @param model Модель с параметрами.
     * @return Имя шаблона.
     */
    @GetMapping("/")
    String listUploadedFiles(Model model);

    /**
     * Выгрузить файл.
     *
     * @param filename Имя файла.
     * @return Ответ с файлом.
     */
    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    ResponseEntity<Resource> serveFile(@PathVariable String filename);

    /**
     * Проверка статуса файла.
     *
     * Метод используется для отладки.
     *
     * @param filename Имя файла.
     * @return Статус файла в виде строки.
     */
    @GetMapping("/statusAsString")
    @ResponseBody
    String checkFileStatus(@RequestParam(required = true) String filename);

    /**
     * Проверка статуса файла.
     *
     * Шаблон по этому пути делает autorefresh каждые 3 секунды.
     *
     * @param filename Имя файла.
     * @param model Модель с параметрами, в которую будет помещён статус.
     * @return Имя шаблона.
     */
    @GetMapping("/status")
    String checkFileStatus(@RequestParam(required = true) String filename, Model model);

    /**
     * Загрузка файла.
     *
     * @param file Файл.
     * @param redirectAttributes Параметры.
     * @return Имя шаблона.
     */
    @PostMapping("/")
    String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes);

    /**
     * Обработчик исключений {StorageFileNotFoundException}.
     *
     * @param exc Исключение.
     * @return Ответ с кодом ошибки 404.
     */
    @ExceptionHandler(StorageFileNotFoundException.class)
    @ResponseBody
    ResponseEntity<Void> handleStorageFileNotFound(StorageFileNotFoundException exc);
}
