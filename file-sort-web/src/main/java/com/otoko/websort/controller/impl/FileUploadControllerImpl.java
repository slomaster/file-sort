package com.otoko.websort.controller.impl;

import com.otoko.websort.controller.FileUploadController;
import com.otoko.websort.service.SortService;
import com.otoko.websort.service.StorageService;
import com.otoko.websort.service.exception.StorageFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Collectors;

/**
 * Реализация контроллера загрузки файлов.
 */
@Controller
public class FileUploadControllerImpl implements FileUploadController {

    /**
     * Сервис хранения.
     */
    private final StorageService storageService;

    /**
     * Сервис сортировки.
     */
    private final SortService sortService;

    @Autowired
    public FileUploadControllerImpl(StorageService storageService, SortService sortService) {
        this.storageService = storageService;
        this.sortService = sortService;
    }

    @Override
    @GetMapping("/")
    public String listUploadedFiles(Model model) {

        model.addAttribute("files", storageService.loadAll().map(
                path -> MvcUriComponentsBuilder.fromMethodName(FileUploadControllerImpl.class,
                        "serveFile", path.getFileName().toString()).build().toString())
                .collect(Collectors.toList()));

        return "uploadForm";
    }

    @Override
    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @Override
    @GetMapping("/statusAsString")
    @ResponseBody
    public String checkFileStatus(@RequestParam(required = true) String filename) {
        return sortService.checkFileStatus(filename);
    }

    @Override
    @GetMapping("/status")
    public String checkFileStatus(@RequestParam(required = true) String filename, Model model) {

        String status = sortService.checkFileStatus(filename);
        model.addAttribute("status", status);
        return "sortStatusForm";
    }

    @Override
    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

        Path path = storageService.store(file);
        sortService.sort(path);
        String filename = file.getOriginalFilename();

        redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + filename + "!");
        redirectAttributes.addAttribute("filename", filename);

        return "redirect:/status";
    }

    @Override
    @ExceptionHandler(StorageFileNotFoundException.class)
    @ResponseBody
    public ResponseEntity<Void> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }
}
