package com.otoko.websort.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Настройки системы хранения.
 */
@ConfigurationProperties(prefix = "storage")
public class StorageProperties {

    /**
     * Каталог для хранения загруженных файлов.
     */
    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
