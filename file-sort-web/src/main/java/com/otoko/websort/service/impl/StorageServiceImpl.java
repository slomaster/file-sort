package com.otoko.websort.service.impl;

import com.otoko.websort.service.exception.StorageException;
import com.otoko.websort.service.exception.StorageFileNotFoundException;
import com.otoko.websort.config.StorageProperties;
import com.otoko.websort.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Реализация сервиса хранения файлов.
 */
@Service
public class StorageServiceImpl implements StorageService {

    private final Path location;

    @Autowired
    public StorageServiceImpl(StorageProperties properties) {
        this.location = Paths.get(properties.getLocation());
    }

    /**
     * Теоретически имени файла может и не быть, в таком случае нужно что-то сгенерировать.
     *
     * @param name Имя файла.
     * @return Итоговое имя файла.
     */
    private String generateIfEmpty(String name) {
        return StringUtils.isEmpty(name) ? UUID.randomUUID().toString() : name;
    }

    @Override
    public Path store(MultipartFile file) {
        String filename = StringUtils.cleanPath(generateIfEmpty(file.getOriginalFilename()));
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException("Cannot store file with relative path outside current directory " + filename);
            }
            try (InputStream inputStream = file.getInputStream()) {
                Path filePath = this.location.resolve(filename);
                Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
                return filePath;
            }
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.location, 1)
                    .filter(path -> !path.equals(this.location))
                    .filter(path -> !path.toString().endsWith(".tmp"))
                    .map(this.location::relativize);
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(String filename) {
        return location.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(location.toFile());
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(location);
        } catch (IOException e) {
            throw new StorageException("Could not initialize service", e);
        }
    }
}
