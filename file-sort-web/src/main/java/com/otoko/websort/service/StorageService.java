package com.otoko.websort.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

/**
 * Сервис работы с файлами.
 */
public interface StorageService {

    /**
     * Инициализация. Создание всех необходимых структур файловой системы если они не существуют.
     *
     * Метод должен быть выполнен перед началом работы.
     */
    void init();

    /**
     * Сохранение загруженного файла на файловой системе.
     *
     * @param file Загруженный файл.
     * @return Путь до сохранённого файла.
     */
    Path store(MultipartFile file);

    /**
     * Получить список путей всех хранящихся в файловой системе файлов.
     *
     * @return Список путей до сохранённых файлов.
     */
    Stream<Path> loadAll();

    /**
     * Получить путь до конкретного хранящегося в файловой системе файла.
     *
     * @param filename Имя файла.
     * @return Путь до хранящегося файла.
     */
    Path load(String filename);

    /**
     * Получить конкретный хранящийся в файловой системе файл как ресурс.
     *
     * @param filename Имя файла.
     * @return Ресурс.
     */
    Resource loadAsResource(String filename);

    /**
     * Очистить хранилище от всех файлов.
     *
     * Может быть полезно в случае когда хранилище используется как временное и работа с ним завершена.
     */
    void deleteAll();
}
