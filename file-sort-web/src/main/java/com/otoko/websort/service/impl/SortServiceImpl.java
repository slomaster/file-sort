package com.otoko.websort.service.impl;

import com.otoko.filesort.FloatFileSortMultiTaskManager;
import com.otoko.filesort.task.SortTask;
import com.otoko.websort.service.SortService;
import com.otoko.websort.service.exception.StorageException;
import com.otoko.websort.service.exception.StorageFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Реализация сервиса сортировки.
 */
@Service
public class SortServiceImpl implements SortService {

    /**
     * Сортировщик.
     */
    private final FloatFileSortMultiTaskManager fileSorter;

    @Autowired
    public SortServiceImpl() {
        fileSorter = new FloatFileSortMultiTaskManager();
    }

    @Override
    public void sort(Path path) {
        String filePathString = StringUtils.cleanPath(path.toString());
        if (!Files.exists(path)) {
            throw new StorageFileNotFoundException("Could not find file " + filePathString);
        }
        String tempFilePathString = filePathString + ".tmp";
        Path tempPath = Paths.get(tempFilePathString);
        try {
            Files.deleteIfExists(tempPath);
        } catch (IOException e) {
            throw new StorageException("Could not delete file " + tempFilePathString, e);
        }
        fileSorter.sortFile(filePathString, tempFilePathString);
    }

    @Override
    public String checkFileStatus(String filename) {
        SortTask task = fileSorter.getTask(filename);
        if (task == null) {
            return "Unknown";
        }
        return task.getStatus();
    }
}
