package com.otoko.websort.service;

import java.nio.file.Path;

/**
 * Сервис сортировки.
 */
public interface SortService {

    /**
     * Сортировка файла по заданному пути.
     *
     * @param path Путь до файла.
     */
    void sort(Path path);

    /**
     * Проверка статуса процесса сортировки файла.
     *
     * @param filename Имя файла.
     * @return Строка со статусом.
     */
    String checkFileStatus(String filename);
}
